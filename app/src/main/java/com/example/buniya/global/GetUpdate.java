package com.example.buniya.global;

import android.app.Application;
import android.os.AsyncTask;

import com.example.buniya.Api.ApiHandler;
import com.example.buniya.Api.ResponsePOJO.HistoryResponsePOJO;
import com.example.buniya.database.entity.HistoryEntity;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.repository.HistoryRepository;
import com.example.buniya.database.repository.LoggedUserRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetUpdate {

   // private JsonParser jsonParser;
    private HistoryRepository historyRepository;
    private LoggedUserRepository loggedUserRepository;

    public GetUpdate(Application application){
       // jsonParser = new JsonParser();
        historyRepository = new HistoryRepository(application);
        loggedUserRepository = new LoggedUserRepository(application);
    }

    public void UpdateHistory(){

       // new UpdateHistory().execute();
        Call<HistoryResponsePOJO> call = ApiHandler.departmentInterface.getHistory(GlobalValue.loggedUser.getUsername(),GlobalValue.loggedUser.getToken());

        call.enqueue(new Callback<HistoryResponsePOJO>() {
            @Override
            public void onResponse(Call<HistoryResponsePOJO> call, Response<HistoryResponsePOJO> response) {
                for(HistoryEntity historyEntity : response.body().getData()){
                    historyRepository.insert(historyEntity);
                }
            }

            @Override
            public void onFailure(Call<HistoryResponsePOJO> call, Throwable t) {

            }
        });
    }

    private class UpdateHistory extends AsyncTask<String,Void,Void>{

        JSONObject jsonObject;
        @Override
        protected Void doInBackground(String... strings) {
            LoggedUser loggedUser = loggedUserRepository.getUser();
            HashMap<String,String> data = new HashMap<>();
            data.put("username",loggedUser.getUsername());
           // jsonObject = jsonParser.performPostCI(GlobalValue.api_link+"history/getHistoryInfo",data);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if(jsonObject!=null){
                JSONArray array = jsonObject.getJSONArray("data");
                for(int i=0;i<array.length();i++){
                    JSONObject jsonObject = array.getJSONObject(i);
                    HistoryEntity historyEntity = new HistoryEntity();
                    historyEntity.setAddress(jsonObject.getString("ipAddress"));
                    historyEntity.setCaller(jsonObject.getString("caller"));
                    historyEntity.setReceiver(jsonObject.getString("receiver"));
                    historyEntity.setPort(Integer.parseInt(jsonObject.getString("port")));
                    historyEntity.setDate(new Date(jsonObject.getString("date")));
                    historyEntity.setId(jsonObject.getInt("id"));
                    historyRepository.insert(historyEntity);
                }
                }
            }catch (JSONException ex){

            }
        }
    }
}
