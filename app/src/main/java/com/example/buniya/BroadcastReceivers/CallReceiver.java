package com.example.buniya.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.FragmentTransaction;

import com.example.buniya.Test;
import com.example.buniya.home.fragements.Friends.ChatWindow;
import com.example.buniya.home.fragements.call.CallReceiverDialog;
import com.example.buniya.interfaces.NavigationHost;

public class CallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String port = intent.getStringExtra("PORT");
        String address = intent.getStringExtra("IPAddress");
        String sessionid = intent.getStringExtra("SessionId");
        String caller = intent.getStringExtra("Caller");
        String type = intent.getStringExtra("Type");
        String username = intent.getStringExtra("Username");
        Intent i = new Intent(context, Test.class);
        i.putExtra("PORT",port);
        i.putExtra("IPAddress",address);
        i.putExtra("Caller",caller);
        i.putExtra("SessionId",sessionid);
        i.putExtra("Type",type);
        i.putExtra("Username",username);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
