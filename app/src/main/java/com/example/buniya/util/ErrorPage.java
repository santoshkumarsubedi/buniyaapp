package com.example.buniya.util;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.R;
import com.example.buniya.login.Login;
import com.google.android.material.button.MaterialButton;


public class ErrorPage extends Fragment {

    String message;
    TextView textView;
    MaterialButton mBback;

    public ErrorPage(String message){
        this.message = message;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_error_page, container, false);
        textView = view.findViewById(R.id.error_message);
        mBback = view.findViewById(R.id.error_buton_back);
        textView.setText(message);
        mBback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) getActivity()).navigateTo(new Login(), false);
            }
        });
        return view;
    }

}
