package com.example.buniya.util;

import java.security.Key;

import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTExtractor {
    public static Claims getClaims(String rowdata){
        KeyGenerator keyGenerator = new KeyGenerator();
        String firstKey=keyGenerator.generateKey();
        Key signingKey = new SecretKeySpec(firstKey.getBytes(), SignatureAlgorithm.HS256.getJcaName());
        Claims claims=null;
        try{
            System.out.println(rowdata);
            claims = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(rowdata).getBody();
            System.out.println(claims);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return claims;
    }
}
