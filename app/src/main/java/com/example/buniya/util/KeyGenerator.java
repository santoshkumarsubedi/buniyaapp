package com.example.buniya.util;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class KeyGenerator {

    public static String generateKey(){
        String allowedChar = "abcdefghijklmnopqrstuvwxyz0123456789+=!@$#";
        String alphabets = "abcdefghijklmnopqrstuvwxyz";
        char[] words = new char[36];
        char[] saltChar = new char[15];
        char[] keyChar = new char[15];
        char[] dateChar = new char[10];
        char[] matA = new char[20];
        char[] alphaChar = new char[26];
        int[] nums = new int[20];
        char[] enc = new char[256];
        words = allowedChar.toCharArray();
        int wordlength = 256;
        int i=0,j=0,k=0;
        String key,salt,date;

        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHH");
        //LocalDateTime dt = LocalDateTime.now();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddkk");
        Date dates = Calendar.getInstance().getTime();

        date = format.format(dates);
       int somevalue = Integer.parseInt(date);
        //int somevalue = 2019112021;
        somevalue = (somevalue-(somevalue/2))/((somevalue*somevalue)%26);
        dateChar = date.toCharArray();
        alphaChar = alphabets.toCharArray();
        for(i=0;i<10;i++){

            try {
                saltChar[i] = alphaChar[((i * somevalue) % 26)];
            }catch (Exception e){
                saltChar[i] = (alphaChar[((i * somevalue) % 26)*(-1)]);
            }
            keyChar[i] = alphaChar[(i*1037/273)%26];
        }
        String temp = String.valueOf(keyChar)+String.valueOf(saltChar);
        matA = temp.toCharArray();


        for(j=0;j<20;j++){
            nums[j] = (matA[j]*matA[j]*Integer.parseInt(String.valueOf(dateChar[j%10])))%26;
        }

        for(i=0; i<wordlength;i++){
            enc[i] = words[(nums[i%19]*Integer.parseInt(String.valueOf(dateChar[j%10]))+i)%42];
        }


        String final_key="";
        for(i=0;i<enc.length;i++){
            final_key+=enc[i];
        }
        return final_key;
    }
}
