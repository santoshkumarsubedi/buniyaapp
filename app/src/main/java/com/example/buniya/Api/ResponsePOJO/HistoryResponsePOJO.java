package com.example.buniya.Api.ResponsePOJO;

import com.example.buniya.database.entity.HistoryEntity;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HistoryResponsePOJO implements Serializable {
   @SerializedName("data")
    private List<HistoryEntity> data;

    public List<HistoryEntity> getData() {
        return data;
    }

    public void setData(List<HistoryEntity> data) {
        this.data = data;
    }
}
