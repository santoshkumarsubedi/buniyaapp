package com.example.buniya.Api.ResponsePOJO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DepartmentListResponsePOJO implements Serializable {

    @SerializedName("data")
    ArrayList<DepartmentResponseModel> data;

    public ArrayList<DepartmentResponseModel> getData() {
        return data;
    }

    public void setData(ArrayList<DepartmentResponseModel> data) {
        this.data = data;
    }
}
