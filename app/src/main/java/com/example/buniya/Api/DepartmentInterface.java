package com.example.buniya.Api;

import com.example.buniya.Api.ResponsePOJO.DepartmentListResponsePOJO;
import com.example.buniya.Api.ResponsePOJO.HistoryResponsePOJO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface DepartmentInterface {

    @POST("department/list")
    Call<DepartmentListResponsePOJO> getDepartmentList(@Header("Authorization") String token);

    @POST("history/getHistoryInfo")
    Call<HistoryResponsePOJO> getHistory(@Body String username,@Header("Authorization") String token);

}
