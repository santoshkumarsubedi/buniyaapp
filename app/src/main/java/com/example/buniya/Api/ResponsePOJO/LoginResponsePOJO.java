package com.example.buniya.Api.ResponsePOJO;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponsePOJO implements Serializable {
    @SerializedName("Status")
    private String status;

    @SerializedName("Username")
    private String username;

    @SerializedName("UserId")
    private int userId;

    @SerializedName("Fullname")
    private String fullName;

    @SerializedName("DepartmentId")
    private int departmentId;

    @SerializedName("Token")
    private String token;

    @SerializedName("PasswordStatus")
    private String passwordStatus;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPasswordStatus() {
        return passwordStatus;
    }

    public void setPasswordStatus(String passwordStatus) {
        this.passwordStatus = passwordStatus;
    }
}
