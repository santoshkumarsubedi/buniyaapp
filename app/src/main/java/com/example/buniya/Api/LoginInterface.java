package com.example.buniya.Api;

import com.example.buniya.Api.RequestPOJO.LoginRequestPOJO;
import com.example.buniya.Api.ResponsePOJO.GeneralResponsePOJO;
import com.example.buniya.Api.ResponsePOJO.LoginResponsePOJO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginInterface {

    @POST("logins")
    Call<LoginResponsePOJO> Login(@Body LoginRequestPOJO loginRequestPOJO);

    @POST("changePassword")
    Call<GeneralResponsePOJO> ChangePassword(@Body LoginRequestPOJO loginRequestPOJO,
                                             @Header("Authorization") String Token);
}
