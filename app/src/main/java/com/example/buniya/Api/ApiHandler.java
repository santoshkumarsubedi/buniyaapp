package com.example.buniya.Api;

import com.example.buniya.Api.RequestPOJO.BugReportRequestPOJO;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHandler {
    private static String api_link = "http://192.168.100.56:8080/AdminPanel/api/";
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(api_link)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static LoginInterface loginInterface = retrofit.create(LoginInterface.class);

    public static DepartmentInterface departmentInterface = retrofit.create(DepartmentInterface.class);

    public static BugReportInterface bugReportInterface = retrofit.create(BugReportInterface.class);
}
