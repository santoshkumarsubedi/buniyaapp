package com.example.buniya.Api.ResponsePOJO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DepartmentResponseModel implements Serializable {
    @SerializedName("department_id")
    private int department_id;

    @SerializedName("department_name")
    private String department_name;

    @SerializedName("userModels")
    private ArrayList<UserResponseModel> userModels;

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public ArrayList<UserResponseModel> getUserModels() {
        return userModels;
    }

    public void setUserModels(ArrayList<UserResponseModel> userModels) {
        this.userModels = userModels;
    }

    public int getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }
}
