package com.example.buniya.Api.RequestPOJO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BugReportRequestPOJO implements Serializable {
    @SerializedName("subject")
    private String subject;

    @SerializedName("report")
    private String report;

    @SerializedName("date")
    private String date;

    @SerializedName("username")
    private String username;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
