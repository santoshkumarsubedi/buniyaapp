package com.example.buniya.Api.ResponsePOJO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GeneralResponsePOJO implements Serializable {

    @SerializedName("Status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
