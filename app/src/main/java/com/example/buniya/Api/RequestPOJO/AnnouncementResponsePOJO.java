package com.example.buniya.Api.RequestPOJO;

import com.example.buniya.model.Announcement;

import java.io.Serializable;
import java.util.List;

public class AnnouncementResponsePOJO implements Serializable {

   private List<Announcement> Data;

    public List<Announcement> getData() {
        return Data;
    }

    public void setData(List<Announcement> data) {
        Data = data;
    }
}
