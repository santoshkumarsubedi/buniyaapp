package com.example.buniya.Api;

import com.example.buniya.Api.RequestPOJO.AnnouncementResponsePOJO;
import com.example.buniya.Api.RequestPOJO.BugReportRequestPOJO;
import com.example.buniya.Api.ResponsePOJO.GeneralResponsePOJO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface BugReportInterface {
    @POST("bug/insertBug")
    Call<GeneralResponsePOJO> insertReport(@Body BugReportRequestPOJO bugReportRequestPOJO, @Header("Authorization") String key);

    @POST("announcement/getAnnouncement")
    Call<AnnouncementResponsePOJO> getAnnouncement(@Header("Authorization") String key);
}
