package com.example.buniya;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.buniya.global.GlobalValue;
import com.example.buniya.home.Home;
import com.example.buniya.interfaces.ActivityHost;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.login.Login;
import com.example.buniya.util.KeyGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements NavigationHost, ActivityHost {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, new Login())
                .commit();
    }


    @Override
    public void navigateTo(Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction =
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment);
        if (addToBackstack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @Override
    public void backTo(boolean addToBackstack) {

    }

    @Override
    public void changeActivity() {
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

}
