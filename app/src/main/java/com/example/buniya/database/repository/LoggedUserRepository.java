package com.example.buniya.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.example.buniya.database.AppDatabase;
import com.example.buniya.database.dao.LoggedUserDao;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.entity.Users;

public class LoggedUserRepository {

    private LoggedUserDao loggedUserDao;

    public LoggedUserRepository(Application application){
        AppDatabase appDatabase = AppDatabase.getAppDatabase(application);
        loggedUserDao = appDatabase.loggedUser();
    }

    public void insert(LoggedUser user){
        new InsertUser().execute(user);
    }
    public LoggedUser getUser(){
        return loggedUserDao.getLoggedUser();
    }

    private class InsertUser extends AsyncTask<LoggedUser,Void,Void> {


        @Override
        protected Void doInBackground(LoggedUser... users) {
            loggedUserDao.insert(users[0]);
            return null;
        }
    }


}
