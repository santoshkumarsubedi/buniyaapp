package com.example.buniya.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "departmentMessage")
public class DepartmentMessage {
    @NotNull @PrimaryKey(autoGenerate =true)
    @ColumnInfo(name = "id")
    private int messageId;

    @ColumnInfo(name = "message")
    private String message;

    @ColumnInfo(name = "sender")
    private String sender;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "time")
    private String time;

    public DepartmentMessage(String message, String sender, String date, String time) {
        this.message = message;
        this.sender = sender;
        this.date = date;
        this.time = time;
    }

    public DepartmentMessage(){};

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }
}
