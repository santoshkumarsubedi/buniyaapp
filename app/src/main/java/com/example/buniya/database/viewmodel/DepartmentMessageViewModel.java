package com.example.buniya.database.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.buniya.database.entity.DepartmentMessage;
import com.example.buniya.database.repository.DepartmentMessageRepository;

import java.util.List;

public class DepartmentMessageViewModel extends AndroidViewModel {
    DepartmentMessageRepository departmentMessageRepository;
    public DepartmentMessageViewModel(@NonNull Application application) {
        super(application);
        departmentMessageRepository = new DepartmentMessageRepository(application);
    }

    public LiveData<List<DepartmentMessage>> getMessages(){
        return departmentMessageRepository.getDepartmentMessages();
    }

    public void insert(DepartmentMessage message){
        departmentMessageRepository.insert(message);
}
}
