package com.example.buniya.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.buniya.database.dao.DepartmentDao;
import com.example.buniya.database.dao.DepartmentMessageDao;
import com.example.buniya.database.dao.HistoryDao;
import com.example.buniya.database.dao.LoggedUserDao;
import com.example.buniya.database.dao.MessageDao;
import com.example.buniya.database.dao.UsersDao;
import com.example.buniya.database.entity.DepartmentEntity;
import com.example.buniya.database.entity.DepartmentMessage;
import com.example.buniya.database.entity.HistoryEntity;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.entity.Message;
import com.example.buniya.database.entity.Users;
import com.example.buniya.util.TypeConverter;

@Database(entities = {DepartmentEntity.class, Users.class, LoggedUser.class, Message.class,
           DepartmentMessage.class,HistoryEntity.class},exportSchema = false,version = 1)
@TypeConverters({TypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract DepartmentDao departmentDao();
    public abstract UsersDao usersDao();
    public abstract LoggedUserDao loggedUser();
    public abstract MessageDao messageDao();
    public abstract DepartmentMessageDao departmentMessageDao();
    public abstract HistoryDao historyDao();

    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(final Context context){
        if(INSTANCE==null){
            synchronized (AppDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class,"database").build();
                }
            }
        }
        return INSTANCE;
    }
}
