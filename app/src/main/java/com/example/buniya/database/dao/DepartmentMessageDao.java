package com.example.buniya.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.buniya.database.entity.DepartmentMessage;

import java.util.List;

@Dao
public interface DepartmentMessageDao {

    @Insert
    void insert(DepartmentMessage message);

    @Query("SELECT * FROM departmentMessage")
    LiveData<List<DepartmentMessage>> getDepartmentMessage();
}
