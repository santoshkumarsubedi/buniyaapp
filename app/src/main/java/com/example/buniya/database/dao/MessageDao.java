package com.example.buniya.database.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.buniya.database.entity.Message;

import java.util.List;


@Dao
public interface MessageDao {
    @Query("SELECT * FROM messages WHERE (messageTo LIKE :friendName OR messageFrom LIKE :friendName)")
    LiveData<List<Message>> getMessageForFriend(String friendName);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertMessage(Message message);
}
