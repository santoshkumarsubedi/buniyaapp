package com.example.buniya.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "history")

public class HistoryEntity {
    @PrimaryKey
    @ColumnInfo(name = "historyId")
    private int id;

    @ColumnInfo(name = "port")
    private int port;

    @ColumnInfo(name = "address")
    private String address;

    @ColumnInfo(name = "caller")
    private String caller;

    @ColumnInfo(name = "receiver")
    private String receiver;

    @ColumnInfo(name = "date")
    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
