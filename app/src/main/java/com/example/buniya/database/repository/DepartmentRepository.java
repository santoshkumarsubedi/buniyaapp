package com.example.buniya.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.example.buniya.database.AppDatabase;
import com.example.buniya.database.dao.DepartmentDao;
import com.example.buniya.database.dao.UsersDao;
import com.example.buniya.database.entity.DepartmentEntity;
import com.example.buniya.database.entity.Users;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.model.DepartmentModel;
import com.example.buniya.model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {
    private DepartmentDao departmentDao;
    private UsersDao usersDao;

    public DepartmentRepository(Application application){
        AppDatabase appDatabase = AppDatabase.getAppDatabase(application);
        departmentDao = appDatabase.departmentDao();
        usersDao = appDatabase.usersDao();
    }

    public Users getUserById(int id){
        return usersDao.getUserByUserId(id);
    }

    public List<Users> getAllUsers(){
        return  usersDao.getUsers();
    }

    public List<DepartmentModel> getAllData(){
        List<DepartmentModel> datas = new ArrayList<>();
        List<DepartmentEntity> departments = departmentDao.getAllDepartments();
        for(DepartmentEntity department:departments){
            DepartmentModel model = new DepartmentModel();
            model.setDepartmentId(department.getDepartmentId());
            model.setDepartmentName(department.getDepartmentName());
            model.setExpanded(false);
            List<Users> users = this.usersDao.getUsersByDepartment(department.getDepartmentId());
            System.out.println(users.size());
            List<UserModel> userModels = new ArrayList<>();
            for(Users user:users){
                if(!user.getUserName().equals(GlobalValue.loggedUser.getUsername())) {
                    UserModel userModel = new UserModel();
                    userModel.setUser_id(user.getUserId());
                    userModel.setUsername(user.getUserName());
                    userModel.setFullname(user.getFullName());
                    userModels.add(userModel);
                }
            }

            model.setUserModels(userModels);
            datas.add(model);
        }
        return datas;
    }



    public DepartmentEntity getDepartmentByID(int ID){
        return this.departmentDao.getDepartmentById(ID);
    }



    public void insertDepartment(DepartmentEntity departmentEntity){
        new InsertDepartment(this.departmentDao).execute(departmentEntity);
    }

    private class InsertDepartment extends AsyncTask<DepartmentEntity,Void,Void> {

        private DepartmentDao departmentDao;
        public InsertDepartment(DepartmentDao departmentDao){
            this.departmentDao = departmentDao;
        }


        @Override
        protected Void doInBackground(DepartmentEntity... departmentEntities) {
            departmentDao.insert(departmentEntities[0]);
            return null;
        }
    }

    public void insertUser(Users user){
        new InsertUser().execute(user);
    }

    private class InsertUser extends  AsyncTask<Users,Void,Void>{
        @Override
        protected Void doInBackground(Users... users) {
            usersDao.insert(users[0]);
            return null;
        }
    }


}
