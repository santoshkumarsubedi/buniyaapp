package com.example.buniya.database.repository;

import android.app.Application;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.buniya.database.AppDatabase;
import com.example.buniya.database.dao.MessageDao;
import com.example.buniya.database.entity.Message;

import java.util.List;

public class MessageRepository {
    private MessageDao messageDao;

    public MessageRepository(Application application){
        AppDatabase appDatabase=AppDatabase.getAppDatabase(application);
        messageDao=appDatabase.messageDao();
    }

    public LiveData<List<Message>> mMessageList(String friendname){
        return messageDao.getMessageForFriend(friendname);
    }

    public void insert(Message message){new AsyncInsert(messageDao).execute(message);}

    private static class AsyncInsert extends AsyncTask<Message, Void, Void> {
        private MessageDao messageDao;
        public AsyncInsert(MessageDao messageDao){this.messageDao=messageDao;}

        @Override
        protected Void doInBackground(Message... messages) {
            Message message = messages[0];
            messageDao.insertMessage(message);
            return null;
        }
    }
}
