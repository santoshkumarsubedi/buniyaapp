package com.example.buniya.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "departments")
public class DepartmentEntity {

    @PrimaryKey @NonNull
    @ColumnInfo(name = "department_id")
    private int departmentId;

    @ColumnInfo(name = "department_name")
    private String departmentName;

    @Ignore
    public DepartmentEntity(int departmentId,String departmentName){
        this.departmentId = departmentId;
        this.departmentName = departmentName;
    }

    public DepartmentEntity(){

    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
