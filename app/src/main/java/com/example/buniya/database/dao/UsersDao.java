package com.example.buniya.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.buniya.database.entity.Users;

import java.util.List;

@Dao
public interface UsersDao {

    @Query("SELECT * FROM users WHERE department_id = :id")
    List<Users> getUsersByDepartment(int id);

    @Query("SELECT * FROM users WHERE user_id=:id")
    Users getUserByUserId(int id);

    @Query("SELECT * FROM users")
    List<Users> getUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Users users);
}
