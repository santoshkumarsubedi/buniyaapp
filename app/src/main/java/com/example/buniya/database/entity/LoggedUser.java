package com.example.buniya.database.entity;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "loggedUser")

public class LoggedUser {
    @PrimaryKey
    @Nullable
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "userid")
    private int userId;

    @ColumnInfo(name = "username")
    private String username;

    @ColumnInfo(name = "fullname")
    private String fullName;

    @ColumnInfo(name="token")
    private String token;

    @ColumnInfo(name = "department_id")
    private int departmentId;


    public LoggedUser(int userId, String username, String fullName, String token, int departmentId,int id) {
        this.userId = userId;
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.token = token;
        this.departmentId = departmentId;
    }

    public LoggedUser(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}
