package com.example.buniya.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.buniya.database.AppDatabase;
import com.example.buniya.database.dao.DepartmentMessageDao;
import com.example.buniya.database.entity.DepartmentMessage;

import java.util.List;

public class DepartmentMessageRepository {
    private DepartmentMessageDao departmentMessageDao;

    public DepartmentMessageRepository(Application application){
        AppDatabase database = AppDatabase.getAppDatabase(application);
        departmentMessageDao= database.departmentMessageDao();
    }

    public void insert(DepartmentMessage message){
        new InsertMessage().execute(message);
    }

    public LiveData<List<DepartmentMessage>> getDepartmentMessages(){
        return departmentMessageDao.getDepartmentMessage();
    }

    private class InsertMessage extends AsyncTask<DepartmentMessage,Void,Void> {

        @Override
        protected Void doInBackground(DepartmentMessage... departmentMessages) {
            departmentMessageDao.insert(departmentMessages[0]);
            return null;
        }
    }


}
