package com.example.buniya.database.viewmodel;

import android.app.Application;


import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.buniya.database.entity.Message;
import com.example.buniya.database.repository.MessageRepository;

import java.util.List;

public class MessageViewModel extends AndroidViewModel {
    private MessageRepository messageRepository;
    public MessageViewModel(@NonNull Application application) {
        super(application);
        this.messageRepository=new MessageRepository(application);
    }

    public LiveData<List<Message>> getListofmessage(String friendName){
        return messageRepository.mMessageList(friendName);
    }

    public void Insert(Message message){
        messageRepository.insert(message);
    }
}
