package com.example.buniya.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "users",foreignKeys = @ForeignKey(entity = DepartmentEntity.class,
                                parentColumns = "department_id",
                                childColumns = "department_id",
                                onDelete = ForeignKey.NO_ACTION))
public class Users {
    @PrimaryKey @NonNull
    @ColumnInfo(name = "user_id")
    private int userId;

    @ColumnInfo(name = "username")
    private String userName;

    @ColumnInfo(name = "fullaname")
    private String fullName;

    @ColumnInfo(name = "department_id")
    private int departmentId;

    public Users(int userId,String userName,String fullName,int departmentId){
        this.userId = userId;
        this.userName = userName;
        this.fullName = fullName;
        this.departmentId = departmentId;
    }

    public Users(){

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}
