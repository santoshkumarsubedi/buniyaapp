package com.example.buniya.database.repository;


import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.buniya.database.AppDatabase;
import com.example.buniya.database.dao.HistoryDao;
import com.example.buniya.database.entity.HistoryEntity;
import com.example.buniya.home.fragements.history.History;

import java.util.List;

public class HistoryRepository {
    private HistoryDao historyDao;

    public HistoryRepository(Application application){
        AppDatabase appDatabase = AppDatabase.getAppDatabase(application);
        historyDao = appDatabase.historyDao();
    }


    public void insert(HistoryEntity historyEntity){
        new InsertHistory(historyDao).execute(historyEntity);
    }

    public LiveData<List<HistoryEntity>> getHistory(){
     return historyDao.getHistoryByUser();
    }

    private class InsertHistory extends AsyncTask<HistoryEntity,Void,Void>{

        private HistoryDao historyDao;
        public InsertHistory(HistoryDao historyDao){
            this.historyDao = historyDao;
        }

        @Override
        protected Void doInBackground(HistoryEntity... historyEntities) {
            this.historyDao.insert(historyEntities[0]);
            return null;
        }
    }

}
