package com.example.buniya.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.buniya.database.entity.HistoryEntity;

import java.util.List;

@Dao
public interface HistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(HistoryEntity historyEntity);

    @Query("SELECT * FROM history")
    LiveData<List<HistoryEntity>> getHistoryByUser();

}
