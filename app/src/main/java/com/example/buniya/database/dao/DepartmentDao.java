package com.example.buniya.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.buniya.database.entity.DepartmentEntity;

import java.util.List;

@Dao
public interface DepartmentDao {
    @Query("SELECT * FROM departments")
    List<DepartmentEntity> getAllDepartments();

    @Query("SELECT * FROM departments WHERE department_id=:ID")
    DepartmentEntity getDepartmentById(int ID);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DepartmentEntity departmentEntity);

}
