package com.example.buniya.home.fragements.call;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.buniya.R;
import com.example.buniya.global.AudioCallFeature;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.services.WebSocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;


public class Calling extends Fragment {

    TextView status,callerid;
    private static final String LOG_TAG="AudioCall";
    Socket socket;
    String loggedUser,callto;
    Gson gson;
    Button endCall;
    String sessionId;
    Activity activity;
    String type;

    public Calling(String loggedUser,String callto,String type){
        this.loggedUser = loggedUser;
        this.callto = callto;
        this.type = type;
        gson = new Gson();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calling, container, false);
        status = view.findViewById(R.id.calling_status);
        callerid = view.findViewById(R.id.call_id);
        endCall = view.findViewById(R.id.calling_end_Call);
        socket= WebSocketService.getSocket();
        HashMap<String,String> map = new HashMap<>();
        activity = getActivity();
        if(type.equals("personal")) {
            map.put("sender", loggedUser);
            map.put("receiver", callto);
            if (socket != null) {
                socket.emit("initialize-call", gson.toJson(map));
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        status.setText("Check If User is online ");
                    }
                });

                socket.on("call-status", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        System.out.println(args[0]);
                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String statu = jsonObject.getString("status");
                            if (statu.equals("NotFound")) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        status.setText(callto + " is not online");
                                    }
                                });

                            } else if (statu.equals("UserBusy")) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        status.setText("User is Busy on another Call");

                                    }
                                });
                                Thread.sleep(500);
                                ((NavigationHost) activity).backTo(false);
                            } else if (statu.equals("Found")) {
                                String ip = jsonObject.getString("IPAddress");
                                AudioCallFeature.address = InetAddress.getByName(ip);
                                String port = jsonObject.getString("PORT");
                                AudioCallFeature.serverPort = Integer.parseInt(port);
                                sessionId = jsonObject.getString("SessionId");
                                //startPermission();
                                activity.runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        status.setText("Connecting to server");

                                    }
                                });

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                });

                socket.on("call-action", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        try {
                            System.out.println(args[0]);
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String action = jsonObject.getString("Action");
                            final String sessionId = jsonObject.getString("SessionId");
                            if (action.equals("accept")) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        status.setText("Connected");
                                        callerid.setText(sessionId);
                                    }
                                });

                                AudioCallFeature.startSpeakers();
                                AudioCallFeature.startMic();
                            } else if (action.equals("reject")) {
                                AudioCallFeature.mic = false;
                                AudioCallFeature.speakers = false;
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        status.setText("Rejected");
                                    }
                                });
                                ((NavigationHost) activity).backTo(false);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                });
            }


        }else if(type.equals("group")){
            map.put("caller",loggedUser);
            map.put("department",callto);
            socket.emit("department-call",new JSONObject(map));
            socket.on("call-status", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try{
                        JSONObject jsonObject = new JSONObject(args[0].toString());
                        String ip = jsonObject.getString("IPAddress");
                        AudioCallFeature.address = InetAddress.getByName(ip);
                        String port = jsonObject.getString("PORT");
                        AudioCallFeature.serverPort = Integer.parseInt(port);
                        sessionId = jsonObject.getString("SessionId");
                        AudioCallFeature.mic = true;
                        AudioCallFeature.speakers = true;
                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                status.setText("Connected");

                            }
                        });
                        AudioCallFeature.startMic();
                        AudioCallFeature.startSpeakers();
                    }catch (JSONException ex){
                        ex.printStackTrace();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        endCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (socket != null) {
                        HashMap<String, String> data = new HashMap<>();
                        data.put("SessionId", sessionId);
                        data.put("Action", "reject");
                        data.put("Type", type);


                            if (socket != null) {
                                if (type.equals("personal")) {
                                    socket.emit("call-action", new JSONObject(data));
                                }
                                else if(type.equals("group")){
                                    socket.emit("call-action-department",new JSONObject(data));
                                }
                                ((NavigationHost) activity).backTo(false);
                            }

                    }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
    }

    public boolean startPermission(){
        boolean start = true;
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            start = false;
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO},10);
        }
        return start;
    }

}
