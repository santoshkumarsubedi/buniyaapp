package com.example.buniya.home.fragements.setting;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.buniya.R;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.home.fragements.setting.Dialogs.SettingDialog;
import com.example.buniya.home.fragements.setting.Dialogs.SettingDialogReport;
import com.example.buniya.home.fragements.setting.Dialogs.SettingNoticeDialog;
import com.google.android.material.button.MaterialButton;
import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class Setting extends Fragment {
    TextView username;
    LoggedUser loggedUser;
    LoggedUserRepository loggedUserRepository;
    MaterialButton select;
    String user_name;
    CircleImageView profilePicture;
    TextView poweredBy;
    MaterialButton notice_board,manual_sync,report_bug,about;

    private static int RESULT_LOAD_IMAGE = 1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        username = view.findViewById(R.id.setting_username);
        select = view.findViewById(R.id.setting_upload_image);
        notice_board = view.findViewById(R.id.button_notice_board);
        manual_sync = view.findViewById(R.id.button_manual_sync);
        report_bug = view.findViewById(R.id.button_report_bugs);
        about = view.findViewById(R.id.button_about);
        loggedUserRepository = new LoggedUserRepository(getActivity().getApplication());
        profilePicture = view.findViewById(R.id.setting_profile_picture);
        poweredBy = view.findViewById(R.id.poweredBy);
        poweredBy.setText(Html.fromHtml("<b><font face='cursive'> Powered By CodePoets</font></b><br/><i><font face='cursive' size='20'><b>make IT work</b></font></i>"));
        new GTUser().execute();

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i,RESULT_LOAD_IMAGE);
            }
        });

        notice_board.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingNoticeDialog().show(getFragmentManager(),"Notice Board");
            }
        });

        report_bug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingDialogReport(user_name).show(getFragmentManager(),"Report Bug");
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SettingDialog().show(getFragmentManager(),"about");
            }
        });



        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            Toast.makeText(getActivity(), picturePath, Toast.LENGTH_SHORT).show();


            uploadImage(picturePath);
        }
    }



    private void uploadImage(String fileLink){
        String baseUrl = GlobalValue.api_link;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        File file = new File(fileLink);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileupload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"),user_name);
        ApiService service = retrofit.create(ApiService.class);
        Call<PostResponse> call = service.postData(fileupload, filename,username);
        System.out.println("Ready to upload");

        //calling the api
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                //hiding progress dialog
                //progressDialog.dismiss();
                System.out.println(response);
                if(response.isSuccessful()){
                    Toast.makeText(getActivity().getApplicationContext(), response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    System.out.println("successfull");
                }
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
               // progressDialog.dismiss();
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(t.getMessage());
            }
        });
    }

    private interface ApiService {
        @Multipart
        @POST("uploadFile")
        Call<PostResponse> postData(
                @Part MultipartBody.Part file, @Part("name") RequestBody name,
                @Part("username") RequestBody username);
    }

    private class PostResponse{
        @SerializedName("success")
        private String success;

        public void setSuccess(String success){
            this.success = success;
        }
        public String getSuccess(){
            return success;
        }
    }

    private class GTUser extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            loggedUser = loggedUserRepository.getUser();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            username.setText(capitalize(loggedUser.getFullName()));
            user_name= loggedUser.getUsername();
            System.out.println("USERNAME:"+user_name);
            Picasso.with(getContext()).load(GlobalValue.api_link+"profile?name="+user_name).into(profilePicture);
        }
    }

    private String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }
}
