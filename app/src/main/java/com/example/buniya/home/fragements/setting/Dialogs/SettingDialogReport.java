package com.example.buniya.home.fragements.setting.Dialogs;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.buniya.Api.ApiHandler;
import com.example.buniya.Api.RequestPOJO.BugReportRequestPOJO;
import com.example.buniya.Api.ResponsePOJO.GeneralResponsePOJO;
import com.example.buniya.R;
import com.example.buniya.global.GlobalValue;
import com.google.android.material.button.MaterialButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingDialogReport extends DialogFragment {

    MaterialButton dismissButton,submit;
    EditText subject,report;
    String username;

    public SettingDialogReport(String username){
        this.username = username;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if(dialog!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width,height);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.layout_report_bug_dialog,container,false);
        dismissButton = view.findViewById(R.id.dismiss_button);
        submit = view.findViewById(R.id.submit_button);
        subject = view.findViewById(R.id.subject_report_bug);
        report = view.findViewById(R.id.report_report_bug);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reportBug = report.getText().toString();
                String reportSubject = report.getText().toString();
                if(reportBug.length()>1&&reportSubject.length()>10){
                    Date date = new Date();
                    SimpleDateFormat datetosend = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                    String sDate = datetosend.format(date);
                    BugReportRequestPOJO data = new BugReportRequestPOJO();
                    data.setDate(sDate);
                    data.setReport(reportBug);
                    data.setSubject(reportSubject);
                    data.setUsername(username);
                    Call<GeneralResponsePOJO> call = ApiHandler.bugReportInterface.insertReport(data,GlobalValue.loggedUser.getToken());
                    call.enqueue(new Callback<GeneralResponsePOJO>() {
                        @Override
                        public void onResponse(Call<GeneralResponsePOJO> call, Response<GeneralResponsePOJO> response) {
                            dismiss();
                        }

                        @Override
                        public void onFailure(Call<GeneralResponsePOJO> call, Throwable t) {
                            dismiss();
                        }
                    });

                    //new InsertReport().execute(reportSubject,reportBug,sDate,username);
                }

            }
        });

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }



}
