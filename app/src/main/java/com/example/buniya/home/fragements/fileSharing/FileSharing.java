package com.example.buniya.home.fragements.fileSharing;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.buniya.R;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.home.fragements.fileSharing.adapter.FileSharingAdapter;
import com.example.buniya.model.FIleSharingModel;
import com.example.buniya.util.FilePath;
import com.example.buniya.util.FileUtils;
import com.example.buniya.util.JsonParsers;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;


public class FileSharing extends Fragment implements FileSharingAdapter.FileSharingClickListener{

    Activity activity;
    RecyclerView recyclerView;
    EditText fileName;
    ImageView choose,upload;
    private static int RESULT_LOAD_IMAGE = 1;
    private String sender,receiver;
    FilePath filePath;
    JsonParsers jsonParser;
    FileSharingAdapter fileSharingAdapter;
    List<FIleSharingModel> models;
    String sFileName;
    private Uri fileUri;
    private String filePaths;


    public FileSharing(String sender,String receiver){
        this.sender = sender;
        this.receiver = receiver;
        filePath = new FilePath();
        jsonParser = new JsonParsers();
        models = new ArrayList<>();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_file_sharing, container, false);
        recyclerView = view.findViewById(R.id.file_sharing_recycler_view);
        fileName = view.findViewById(R.id.file_sharing_filename);
        choose = view.findViewById(R.id.file_sharing_choose_file);
        upload = view.findViewById(R.id.file_sharing_upload_file);
        fileSharingAdapter = new FileSharingAdapter(models,sender,getContext(),this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(fileSharingAdapter);
        //jsonParser = new JsonParser();
        new getFileSharing().execute();
        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }else{
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("*/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(intent, 1);
                    }
                }else {
                    //startPermissionAudioMic();

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(intent, 1);
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file = fileName.getText().toString();
                if(!file.equals(""))
                    uploadFile(file);
                else
                    Toast.makeText(activity, "please select a file to upload", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }

    /*public void getPath(Uri uri){
        try {

            Cursor cursor = getActivity().getContentResolver().query(uri,null,null,null,null);
            cursor.moveToFirst();
            String[] fileColumn = {MediaStore.}
            int columnid = cursor.getColumnIndex(0)
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":")+1);
            fileName.setText(document_id);
            cursor.close();
        }catch(Exception e){

        }
    }*/
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== 1) {

                    fileUri = data.getData();
                    //Log.i(TAG, "Uri = " + uri.toString());
        fileName.setText(fileUri.toString());


        }
    }


    private void uploadFile(String fileLink){
        String baseUrl = GlobalValue.api_link;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        File file = FileUtils.getFile(getActivity(),fileUri);
        Uri path = Uri.fromFile(file);
        String sExtension = MimeTypeMap.getFileExtensionFromUrl(path.toString());
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileupload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody sender = RequestBody.create(MediaType.parse("text/plain"),this.sender);
        RequestBody receiver = RequestBody.create(MediaType.parse("text/plain"),this.receiver);
        RequestBody extension = RequestBody.create(MediaType.parse("text/plain"),sExtension);
        FileSharing.ApiService service = retrofit.create(FileSharing.ApiService.class);
        Call<FileSharing.PostResponse> call = service.postData(fileupload, filename,sender,receiver,extension);
        //calling the api
        call.enqueue(new Callback<FileSharing.PostResponse>() {
            @Override
            public void onResponse(Call<FileSharing.PostResponse> call, Response<FileSharing.PostResponse> response) {
                //hiding progress dialog
                //progressDialog.dismiss();
                System.out.println(response);
                if(response.isSuccessful()){
                    Toast.makeText(getActivity().getApplicationContext(), response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    System.out.println("successfull");
                }
            }
            @Override
            public void onFailure(Call<FileSharing.PostResponse> call, Throwable t) {
                // progressDialog.dismiss();
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                System.out.println(t.getMessage());
            }
        });
    }

    @Override
    public void DownloadClicked(int fileid,String name) {
        //Toast.makeText(activity, fileid, Toast.LENGTH_SHORT).show();
       // System.out.println(fileid);
        System.out.println(fileid);
        String baseUrl = GlobalValue.api_link;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FileSharing.ApiService service = retrofit.create(FileSharing.ApiService.class);
        Call<ResponseBody> call = service.downloadFileWithDynamicUrlSync(GlobalValue.api_link+"getFile?id="+fileid);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                File futureStudioIconFile = new File(getActivity().getExternalFilesDir(null)
                                                                    + File.separator + name);
                InputStream inputStream = null;
                OutputStream outputStream = null;
                try {
                    try {
                        byte[] fileReader = new byte[4096];
                        long fileSize = response.body().contentLength();
                        long fileSIzeDownloaded = 0;
                        inputStream = response.body().byteStream();
                        outputStream = new FileOutputStream(futureStudioIconFile);
                        while (true) {
                            int read = inputStream.read(fileReader);
                            if (read == -1) {
                                break;
                            }
                            outputStream.write(fileReader, 0, read);
                            fileSIzeDownloaded += read;
                        }
                        outputStream.flush();
                    } catch (Exception ex) {
                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        if(outputStream!=null){
                            outputStream.close();
                        }
                    }
                }catch (Exception ex){ }
                Toast.makeText(getActivity(), "File Downloaded Successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println(t.getMessage());
                Toast.makeText(getActivity(), "File Download failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private interface ApiService {
        @Multipart
        @POST("uploadFileShare")
        Call<FileSharing.PostResponse> postData(
                @Part MultipartBody.Part file,
                @Part("name") RequestBody name,
                @Part("sender") RequestBody sender,
                @Part("receiver") RequestBody receiver,
                @Part("extension") RequestBody extension);

        @GET
        Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);
    }

    private class PostResponse{
        @SerializedName("success")
        private String success;

        public void setSuccess(String success){
            this.success = success;
        }
        public String getSuccess(){
            return success;
        }
    }

    class getFileSharing extends AsyncTask<String,String,String> {

        JSONObject jsonObject;
        @Override
        protected String doInBackground(String... strings) {
            HashMap<String,String> data = new HashMap<>();
            data.put("username",sender);

            jsonObject = jsonParser.registerUser(GlobalValue.api_link+"getAllFiles",data);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonObject!=null){
                try {
                    List<FIleSharingModel> models = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        FIleSharingModel fileSharingModel = new FIleSharingModel();
                        fileSharingModel.setExtension(object.getString("extension"));
                        fileSharingModel.setFileName(object.getString("fileName"));
                        fileSharingModel.setReceiver(object.getString("receiver"));
                        fileSharingModel.setSender(object.getString("sender"));
                        fileSharingModel.setTime(object.getString("date"));
                        fileSharingModel.setId(object.getInt("id"));
                        models.add(fileSharingModel);
                    }
                    fileSharingAdapter.setfIleSharingModels(models);
                }catch (Exception ex){

                }
            }
        }
    }
}
