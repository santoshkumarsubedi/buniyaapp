package com.example.buniya.home.fragements.Friends;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;

import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.R;
import com.example.buniya.database.repository.DepartmentRepository;
import com.example.buniya.home.fragements.Friends.adapter.FriendListAdapter;
import com.example.buniya.home.fragements.Friends.adapter.SubItemAdapter;
import com.example.buniya.model.DepartmentModel;

import java.util.ArrayList;
import java.util.List;


public class FriendList extends Fragment implements SubItemAdapter.FriendlistSubInterface {
    RecyclerView recyclerView;
    FriendListAdapter friendListAdapter;
    List<String> list= new ArrayList<>();
    List<DepartmentModel> dataset;
    DepartmentRepository departmentRepository;
    Activity activity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dataset = new ArrayList<>();
        friendListAdapter = new FriendListAdapter(this,dataset,this.getContext());
        activity = getActivity();
        departmentRepository = new DepartmentRepository(activity.getApplication());
        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);
        recyclerView = view.findViewById(R.id.friend_list_recycler_view);
        RecyclerView.LayoutManager mlayoutmanager = new LinearLayoutManager(activity.getApplicationContext());
        recyclerView.setLayoutManager(mlayoutmanager);
        recyclerView.setAnimation(new AnimationSet(true));
        recyclerView.setAdapter(friendListAdapter);
        new DatabaseAsync().execute();
        return view;

    }

    @Override
    public void clickUser(int user_id) {
        ((NavigationHost) activity).navigateTo(new ChatWindow(user_id), false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       // new FetchDepartment().execute();
       // dataset=departmentRepository.getAllData();
        //friendListAdapter.notifyDataSetChanged();
        activity = getActivity();
        new DatabaseAsync().execute();

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new DatabaseAsync().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("resume");
        new DatabaseAsync().execute();
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("Pause");
    }

     class DatabaseAsync extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids){
            dataset = departmentRepository.getAllData();
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid){
            super.onPostExecute(aVoid);
            friendListAdapter.setDataset(dataset);
            friendListAdapter.notifyDataSetChanged();
        }
    }

}
