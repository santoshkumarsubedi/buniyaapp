package com.example.buniya.home.fragements.Friends.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.R;
import com.example.buniya.model.DepartmentModel;

import java.util.List;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.FriendListViewHolder>{
    private List<DepartmentModel> dataset;
    private SubItemAdapter.FriendlistSubInterface mListener;
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    private Context context;

    public FriendListAdapter(SubItemAdapter.FriendlistSubInterface mListener, List<DepartmentModel> dataset, Context context){

        this.dataset = dataset;
        this.mListener = mListener;
        this.context = context;
    }

    @NonNull
    @Override
    public FriendListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.department_list,parent,false);
        return new FriendListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendListViewHolder holder, int position) {
        DepartmentModel set = dataset.get(position);
        holder.textView.setText(set.getDepartmentName());
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                holder.recyclerView.getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );
        if(set.getUserModels()!=null) {
            layoutManager.setInitialPrefetchItemCount(set.getUserModels().size());
        }
        SubItemAdapter subItemAdapter = new SubItemAdapter(mListener,set.getUserModels(),context);
        holder.recyclerView.setAdapter(subItemAdapter);
        holder.recyclerView.setLayoutManager(layoutManager);
        holder.recyclerView.setRecycledViewPool(viewPool);
        holder.recyclerView.setVisibility(set.isExpanded()?View.VISIBLE:View.GONE);
    }

    @Override
    public int getItemCount()
    {
        return dataset.size();
    }

    public void setDataset(List<DepartmentModel> dataset){
        this.dataset = dataset;
        this.notifyDataSetChanged();
    }



    public  class FriendListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView textView;
        public RecyclerView recyclerView;
        public FriendListViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.department_list_name);
            recyclerView = itemView.findViewById(R.id.department_list_recyclerview);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            int adapterposition = getAdapterPosition();
            dataset.get(adapterposition).setExpanded(!dataset.get(adapterposition).isExpanded());
            notifyDataSetChanged();

        }
    }

    public interface RecycleViewClickListener{
        void onClick(int position);
    }
}
