package com.example.buniya.home.fragements.Friends.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.buniya.R;
import com.example.buniya.database.entity.Message;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.home.fragements.department.adapter.DepartmentMessageAdapter;
import com.squareup.picasso.Picasso;
import java.text.SimpleDateFormat;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatWindowAdapter extends RecyclerView.Adapter {
    private List<Message> messages;
    private String receiver;
    private Context context;

    public ChatWindowAdapter(String receiver, List<Message> messages, Context context){
        this.messages = messages;
        this.receiver = receiver;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==0){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.incoming_message,parent,false);
            return new ChatWindowAdapter.IncomingMessageViewHolders(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outgoing_message,parent,false);
            return new ChatWindowAdapter.OutgoingMessageViewHolders(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(messages!=null){
            Message message = messages.get(position);
            SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
            String sTime = time.format(message.getDate_time());
            switch (holder.getItemViewType()){
                case 1:
                    ((ChatWindowAdapter.OutgoingMessageViewHolders) holder).username.setText(message.getMessage());
                    ((ChatWindowAdapter.OutgoingMessageViewHolders) holder).time.setText(sTime);
                    break;
                case 0:
                    ((ChatWindowAdapter.IncomingMessageViewHolders) holder).username.setText(message.getMessageFrom());
                    ((ChatWindowAdapter.IncomingMessageViewHolders) holder).message.setText(message.getMessage());
                    ((ChatWindowAdapter.IncomingMessageViewHolders) holder).time.setText(sTime);
                    Picasso.with(context).load(GlobalValue.api_link+"profile?name="+
                            message.getMessageFrom()).into((((IncomingMessageViewHolders) holder).imageView));
                    break;
            }

        }
    }

    @Override
    public int getItemCount() {
        if(messages!=null){
            return messages.size();
        }else{
            return 0;
        }
    }

    public class OutgoingMessageViewHolders extends RecyclerView.ViewHolder{
        private TextView username;
        private TextView time;
        public OutgoingMessageViewHolders(View itemView) {
            super(itemView);
            username=itemView.findViewById(R.id.outgoing_message_username);
            time = itemView.findViewById(R.id.outgoing_message_time);
        }
    }

    public class IncomingMessageViewHolders extends RecyclerView.ViewHolder{
        public TextView username;
        public TextView message;
        public TextView time;
        public CircleImageView imageView;
        public IncomingMessageViewHolders(View itemView) {
            super(itemView);
            username=itemView.findViewById(R.id.incoming_message_username);
            message=itemView.findViewById(R.id.incoming_message_message);
            time=itemView.findViewById(R.id.incoming_message_time);
            imageView = itemView.findViewById(R.id.image_message_profile);
        }
    }

    public void setMessages(List<Message> messages){
        this.messages = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(messages.size()>0) {
            Message message = messages.get(position);
            System.out.println(message.getMessageTo()+":"+message.getMessageFrom());
            if (receiver.equals(message.getMessageTo())) {
                return 1;
            } else {
                return 0;
            }
        }
        return 0;

    }

    public void setFriendName(String receiver){
        this.receiver = receiver;
    }
}
