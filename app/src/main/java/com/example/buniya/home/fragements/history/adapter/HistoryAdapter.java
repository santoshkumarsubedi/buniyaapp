package com.example.buniya.home.fragements.history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.R;
import com.example.buniya.database.entity.HistoryEntity;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.home.fragements.Friends.adapter.ChatWindowAdapter;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewModel> {

    List<HistoryEntity> histories;
    String username;
    Context context;

    public HistoryAdapter(List<HistoryEntity> histories, String username, Context context){
        this.histories = histories;
        this.username = username;
        this.context = context;
    }

    @NonNull
    @Override
    public HistoryViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_history,parent,false);
        return new HistoryViewModel(view);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setHistories(List<HistoryEntity> histories){
        this.histories = histories;
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(@NonNull HistoryViewModel holder, int position) {
        HistoryEntity history = histories.get(position);
        if(getItemCount()>0){
            SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
            String sTime = time.format(history.getDate());
            if(getItemViewType(position)==1){
                holder.usernames.setText(history.getCaller());
                holder.call_status.setImageResource(R.drawable.call_received);
                Picasso.with(context).load(GlobalValue.api_link+"profile?name="+
                        history.getCaller()).into(holder.profilePic);
            }else if(getItemViewType(position)==0){
                holder.usernames.setText(history.getReceiver());
                holder.call_status.setImageResource(R.drawable.call_made);
                Picasso.with(context).load(GlobalValue.api_link+"profile?name="+
                        history.getReceiver()).into(holder.profilePic);
            }
            holder.dateAndtime.setText(sTime);
        }
    }

    @Override
    public int getItemCount() {
        if(histories!=null){
            return histories.size();
        }else{
            return 0;
        }
    }

    public class HistoryViewModel extends RecyclerView.ViewHolder{
        public ImageView profilePic;
        public ImageView call_status;
        public TextView usernames;
        public TextView dateAndtime;


        public HistoryViewModel(@NonNull View itemView) {
            super(itemView);
            profilePic = itemView.findViewById(R.id.image_history_profile);
            call_status = itemView.findViewById(R.id.image_call);
            usernames = itemView.findViewById(R.id.call_history_username);
            dateAndtime = itemView.findViewById(R.id.incoming_call_time);
        }
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        HistoryEntity historyEntity = histories.get(position);
        if(username.equals(historyEntity.getCaller()))
            return 0;
        else if(username.equals(historyEntity.getReceiver()))
            return 1;

        return 2;
    }
}
