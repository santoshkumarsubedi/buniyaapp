package com.example.buniya.home.fragements.fileSharing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.R;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.home.fragements.Friends.adapter.ChatWindowAdapter;
import com.example.buniya.model.FIleSharingModel;
import com.github.nkzawa.socketio.client.On;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FileSharingAdapter extends RecyclerView.Adapter{

    private List<FIleSharingModel> fIleSharingModels;
    private String username;
    private Context context;
    private FileSharingClickListener clickListener;

    public FileSharingAdapter(List<FIleSharingModel> fIleSharingModels,String username,Context context,FileSharingClickListener clickListener){
        this.fIleSharingModels = fIleSharingModels;
        this.username = username;
        this.context = context;
        this.clickListener = clickListener;
    }
    public void setfIleSharingModels(List<FIleSharingModel> fIleSharingModels){
        this.fIleSharingModels = fIleSharingModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 0){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_file_incoming,parent,false);
            return new FileSharingAdapter.FileSharingViewModelIncomingFIle(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_file_outgoing,parent,false);
            return new FileSharingAdapter.FileSharingVIewModelOutgoingFIle(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(fIleSharingModels!=null){
            FIleSharingModel fileSharingModel = fIleSharingModels.get(position);
            switch (holder.getItemViewType()){
                case 0:
                    switch (fileSharingModel.getExtension()){
                        case "pdf":
                            ((FileSharingViewModelIncomingFIle) holder).fileType.setImageDrawable(context.getResources().getDrawable(R.drawable.pdf_logo));
                    }

                    ((FileSharingViewModelIncomingFIle) holder).download.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    Picasso.with(context).load(GlobalValue.api_link+"profile?name="+fileSharingModel.getSender()).into(((FileSharingViewModelIncomingFIle) holder).profilepic);
                    ((FileSharingViewModelIncomingFIle) holder).filename.setText(fileSharingModel.getFileName());
                    ((FileSharingViewModelIncomingFIle) holder).time.setText(fileSharingModel.getTime());
                    break;
                case 1:
                    ((FileSharingVIewModelOutgoingFIle) holder).download.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    ((FileSharingVIewModelOutgoingFIle) holder).filename.setText(fileSharingModel.getFileName());
                    switch (fileSharingModel.getExtension()){
                        case "pdf":
                            ((FileSharingVIewModelOutgoingFIle) holder).fileType.setImageDrawable(context.getResources().getDrawable(R.drawable.pdf_logo));
                    }

                    ((FileSharingVIewModelOutgoingFIle) holder).time.setText(fileSharingModel.getTime());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return fIleSharingModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        if(getItemCount()>0){
            FIleSharingModel fIleSharingModel = fIleSharingModels.get(position);
            if(fIleSharingModel.getSender().equals(username)){
                return 0;
            }else{
                return 1;
            }
        }
        return 0;
    }

    public class FileSharingViewModelIncomingFIle extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView filename,time;
        ImageView profilepic;
        CircleImageView fileType;
        ImageView download;

        public FileSharingViewModelIncomingFIle(@NonNull View itemView) {
            super(itemView);
            filename = itemView.findViewById(R.id.incoming_message_message);
            profilepic = itemView.findViewById(R.id.image_message_profile);
            time = itemView.findViewById(R.id.incoming_message_time);
            fileType = itemView.findViewById(R.id.incoming_file_file_type);
            download = itemView.findViewById(R.id.incoming_file_download_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            System.out.println("Clicked");
            clickListener.DownloadClicked(fIleSharingModels.get(getAdapterPosition()).getId(),fIleSharingModels.get(getAdapterPosition()).getFileName());
        }
    }

    public class FileSharingVIewModelOutgoingFIle extends RecyclerView.ViewHolder{
        CircleImageView fileType;
        TextView filename, time;
        ImageView download;

        public FileSharingVIewModelOutgoingFIle(@NonNull View itemView) {
            super(itemView);
            fileType = itemView.findViewById(R.id.outgoing_file_type);
            filename = itemView.findViewById(R.id.outgoing_message_username);
            time = itemView.findViewById(R.id.outgoing_message_time);
            download = itemView.findViewById(R.id.outgoing_file_download);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("Clicked");
                    clickListener.DownloadClicked(fIleSharingModels.get(getAdapterPosition()).getId(),fIleSharingModels.get(getAdapterPosition()).getFileName());
                }
            });
        }


    }

        public interface FileSharingClickListener{
            void DownloadClicked(int fileid,String name);
        }
    }

