package com.example.buniya.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.example.buniya.Api.ApiHandler;
import com.example.buniya.Api.ResponsePOJO.DepartmentListResponsePOJO;
import com.example.buniya.Api.ResponsePOJO.DepartmentResponseModel;
import com.example.buniya.Api.ResponsePOJO.UserResponseModel;
import com.example.buniya.BroadcastReceivers.CallReceiver;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.entity.Message;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.database.repository.MessageRepository;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.R;
import com.example.buniya.database.entity.DepartmentEntity;
import com.example.buniya.database.entity.Users;
import com.example.buniya.database.repository.DepartmentRepository;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.services.WebSocketService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends AppCompatActivity implements NavigationHost {

    DepartmentRepository departmentRepository;
    LoggedUserRepository loggedUserRepository;
    MessageRepository messageRepository;
    CallReceiver callReceiver;
    LoggedUser loggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);

        departmentRepository = new DepartmentRepository(getApplication());
        loggedUserRepository = new LoggedUserRepository(getApplication());
        messageRepository = new MessageRepository(getApplication());
        //loggedUser = loggedUserRepository.getUser();
        //new FetchDepartment().execute(KeyGenerator.generateKey());
         getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.home_container, new HomeFragment())
                .commit();

        Call<DepartmentListResponsePOJO> call = ApiHandler.departmentInterface.getDepartmentList(GlobalValue.loggedUser.getToken());
        call.enqueue(new Callback<DepartmentListResponsePOJO>() {
            @Override
            public void onResponse(Call<DepartmentListResponsePOJO> call, Response<DepartmentListResponsePOJO> response) {
                DepartmentListResponsePOJO data = response.body();
                for(DepartmentResponseModel model:data.getData()){
                    System.out.println(model.getDepartment_name());
                    DepartmentEntity departmentModel = new DepartmentEntity();
                    departmentModel.setDepartmentName(model.getDepartment_name());
                    departmentModel.setDepartmentId(model.getDepartment_id());
                    departmentRepository.insertDepartment(departmentModel);
                    for(UserResponseModel usermodel:model.getUserModels()){
                        if(!usermodel.getUsername().equals(GlobalValue.loggedUser.getUsername())) {
                            Users us = new Users();
                            us.setFullName(usermodel.getFullname());
                            us.setUserName(usermodel.getUsername());
                            us.setUserId(usermodel.getUserid());
                            us.setDepartmentId(model.getDepartment_id());
                            departmentRepository.insertUser(us);
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<DepartmentListResponsePOJO> call, Throwable t) {
                System.out.println("failed to fetch department List:"+t.getMessage());
            }
        });

         callReceiver = new CallReceiver();
        //new FetchDepartment().execute();
       // new GetOfflineMessage().execute();
        startPermission();
        //startPermissionAudioRecord();
        //startPermissionAudioMic();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            Intent intent = new Intent(this, WebSocketService.class);
            startService(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("Someone.Calling");
        registerReceiver(callReceiver,filter);

    }

    @Override
    public void navigateTo(Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction =
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.home_container, fragment);

        if (addToBackstack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

    @Override
    public void backTo(boolean addToBackstack) {
        FragmentManager transaction = getSupportFragmentManager();
        if(transaction.getBackStackEntryCount()>0){
            transaction.popBackStack();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReceiver);
    }

    public boolean startPermission(){
        boolean start = true;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            start = false;
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_EXTERNAL_STORAGE},10);
        }
        //startPermissionAudioMic();
        return start;
    }
    public boolean startPermissionAudioRecord(){
        boolean start = true;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            start = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},10);

        }
        return start;
    }
    public boolean startPermissionAudioMic(){
        boolean start = true;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            start = false;
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},10);
        }
        startPermissionAudioRecord();
        return start;
    }

   /* private class GetOfflineMessage extends AsyncTask<Void,Void,Void>{

        JSONObject jsonObject;

        @Override
        protected Void doInBackground(Void... voids) {
            loggedUser = loggedUserRepository.getUser();
            HashMap<String,String> data = new HashMap<>();
            data.put("user_id",String.valueOf(loggedUser.getUserId()));
            jsonObject = jsonParser.performPostCI(GlobalValue.api_link+"department-message/getOfflineMessage",data);
            return null;
        }



        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(jsonObject!=null){
                try {
                    String status = jsonObject.get("Status").toString();
                    if(status.equals("Success")){
                        JSONArray arrayList = jsonObject.getJSONArray("data");
                        for (int i=0;i<arrayList.length();i++){
                            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                            JSONObject data = arrayList.getJSONObject(i);
                            Message message = new Message();
                            message.setMessage(data.get("Message").toString());
                            message.setDate_time(format.parse(data.get("Data").toString()));
                            message.setMessageFrom(data.get("Sender").toString());
                            message.setMessageTo(data.get("Receiver").toString());
                            messageRepository.insert(message);
                        }
                    }

                }catch (Exception ex){

                }

            }else{
                Toast.makeText(Home.this, "Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }*/
}
