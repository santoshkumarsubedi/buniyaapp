package com.example.buniya.home.fragements.setting.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.R;
import com.example.buniya.home.fragements.history.adapter.HistoryAdapter;
import com.example.buniya.model.Announcement;

import java.util.List;

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NoticeAdapterViewHolder> {

    List<Announcement> announcements;
    public NoticeAdapter(List<Announcement> announcements){
        this.announcements = announcements;
    }

    @NonNull
    @Override
    public NoticeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notice,parent,false);
        return new NoticeAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoticeAdapterViewHolder holder, int position) {
        Announcement announcement = announcements.get(position);
        holder.date.setText(announcement.getDate().toString());

        holder.subject.setText(announcement.getSubject());

        if(announcement.isToggle()){
            holder.toggle.setImageResource(R.drawable.keyboard_arrow_up);
            holder.notice.setText("");
            holder.notice.setVisibility(View.INVISIBLE);
        }else{
            holder.toggle.setImageResource(R.drawable.down);
            holder.notice.setText(Html.fromHtml(announcement.getAnnouncement()));
            holder.notice.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return announcements.size();
    }

    public class NoticeAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView subject,date,notice;
        ImageView toggle;

        public NoticeAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            subject = itemView.findViewById(R.id.notice_subject);
            date = itemView.findViewById(R.id.notice_date);
            notice = itemView.findViewById(R.id.notice_content);
            toggle = itemView.findViewById(R.id.notice_toggle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            announcements.get(position).setToggle(!announcements.get(position).isToggle());
            notifyDataSetChanged();
        }
    }

    public void setAnnouncements(List<Announcement> announcements){
        this.announcements = announcements;
        notifyDataSetChanged();
    }
}
