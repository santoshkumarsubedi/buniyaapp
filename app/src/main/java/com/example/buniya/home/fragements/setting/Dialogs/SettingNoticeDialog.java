package com.example.buniya.home.fragements.setting.Dialogs;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.Api.ApiHandler;
import com.example.buniya.Api.RequestPOJO.AnnouncementResponsePOJO;
import com.example.buniya.R;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.home.fragements.setting.adapter.NoticeAdapter;
import com.example.buniya.model.Announcement;
import com.google.android.material.button.MaterialButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingNoticeDialog extends DialogFragment {
    MaterialButton dismiss;
    RecyclerView recyclerView;
    NoticeAdapter adapter;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog =getDialog();
        if(dialog!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width,height);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.layout_notice_dialog,container,false);
        dismiss = view.findViewById(R.id.dismiss_button);
        List<Announcement> announcements = new ArrayList<>();
        adapter = new NoticeAdapter(announcements);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
               getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );
        recyclerView = view.findViewById(R.id.notice_recycle_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        //new GetAnnouncement().execute();
        Call<AnnouncementResponsePOJO> call = ApiHandler.bugReportInterface.getAnnouncement(GlobalValue.loggedUser.getToken());
        call.enqueue(new Callback<AnnouncementResponsePOJO>() {
            @Override
            public void onResponse(Call<AnnouncementResponsePOJO> call, Response<AnnouncementResponsePOJO> response) {
                System.out.println(response.body().getData().size());
                    List<Announcement> data = new ArrayList<>();
                    for(Announcement announcement:response.body().getData()){
                        announcement.setToggle(true);
                        data.add(announcement);
                    }
                    adapter.setAnnouncements(data);
                }




            @Override
            public void onFailure(Call<AnnouncementResponsePOJO> call, Throwable t) {

            }
        });
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

   /* private class GetAnnouncement extends AsyncTask<String,String,String>{

        JSONObject jsonObject;
        @Override
        protected String doInBackground(String... strings) {
            HashMap<String,String> data = new HashMap<>();
            jsonObject = jsonParser.performPostCI(GlobalValue.api_link+"/announcement/getAnnouncement",data);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonObject!=null){
                try {
                    String status = jsonObject.getString("Status");
                    if(status.equals("Success")){
                        List<Announcement> announcements = new ArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONArray("Data");
                        for(int i=0;i<jsonArray.length();i++){
                            Announcement announcement = new Announcement();
                            JSONObject object = jsonArray.getJSONObject(i);
                            announcement.setToggle(true);
                            announcement.setSubject(object.getString("subject"));
                            announcement.setAnnouncement(object.getString("announcement"));
                            announcement.setDate(new Date(object.getString("date")));
                            announcements.add(announcement);
                        }

                        adapter.setAnnouncements(announcements);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/


}
