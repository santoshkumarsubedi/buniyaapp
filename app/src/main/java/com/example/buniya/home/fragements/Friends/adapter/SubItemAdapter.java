package com.example.buniya.home.fragements.Friends.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.R;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SubItemAdapter extends RecyclerView.Adapter<SubItemAdapter.SubItemHolder> {

    private List<UserModel> dataset;
    private FriendlistSubInterface mlistner;
    private Context context;

    public SubItemAdapter(FriendlistSubInterface mlistener,List<UserModel> dataset,Context context){
        this.dataset = dataset;
        this.mlistner = mlistener;
        this.context = context;
    }

    @NonNull
    @Override
    public SubItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list,parent,false);
        return new SubItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubItemHolder holder, int position) {
        holder.tVusername.setText(dataset.get(position).getFullname());
        System.out.println(dataset.get(position).getUsername());
        Picasso.with(context).load(GlobalValue.api_link+"profile?name="+
                dataset.get(position).getUsername()).into(holder.profile);

    }

    @Override
    public int getItemCount() {

            return dataset.size();

    }

    public interface FriendlistSubInterface{
        void clickUser(int userId);
    }

    public class SubItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tVusername;
        CircleImageView profile;

        public SubItemHolder(@NonNull View itemView) {
            super(itemView);
            tVusername = itemView.findViewById(R.id.user_list_username);
            profile = itemView.findViewById(R.id.user_profile);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mlistner.clickUser(dataset.get(getAdapterPosition()).getUser_id());
        }
    }
}
