package com.example.buniya.home;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.buniya.R;
import com.example.buniya.home.fragements.Friends.FriendList;
import com.example.buniya.home.fragements.history.History;
import com.example.buniya.home.fragements.setting.Setting;
import com.example.buniya.home.fragements.department.Department;
import com.example.buniya.home.handlers.TabAdapter;
import com.google.android.material.tabs.TabLayout;

public class HomeFragment extends Fragment {

    private TabAdapter tabAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons={R.drawable.ic_menu_camera,R.drawable.ic_menu_slideshow,R.drawable.ic_menu_manage,R.drawable.ic_menu_gallery};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tabs);
        tabAdapter = new TabAdapter(getActivity().getSupportFragmentManager(),2,getContext());
        tabAdapter.addFragment(new FriendList(),"Friends",tabIcons[0]);
        tabAdapter.addFragment(new History(),"History",tabIcons[1]);
        tabAdapter.addFragment(new Department(),"Department",tabIcons[3]);
        tabAdapter.addFragment(new Setting(),"Setting",tabIcons[2]);
        viewPager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(viewPager);
        highlightCurrentTab(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                highlightCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return view;
    }

    private void highlightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(tabAdapter.getTabView(i));
        }

        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(tabAdapter.getSelectedTabView(position));

    }

}
