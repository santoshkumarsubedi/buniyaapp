package com.example.buniya.home.fragements.department;


import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.R;
import com.example.buniya.database.entity.DepartmentEntity;
import com.example.buniya.database.entity.DepartmentMessage;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.repository.DepartmentRepository;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.database.viewmodel.DepartmentMessageViewModel;
import com.example.buniya.home.fragements.Friends.ChatWindow;
import com.example.buniya.home.fragements.call.Calling;
import com.example.buniya.home.fragements.department.adapter.DepartmentMessageAdapter;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.services.WebSocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Department extends Fragment {
    TextView departmentName;
    DepartmentEntity department;
    DepartmentRepository departmentRepository;
    LoggedUser loggedUser;
    LoggedUserRepository loggedUserRepository;
    EditText messageBox;
    ImageView sendButton;
    private RecyclerView recyclerView;
    DepartmentMessageAdapter adapter;
    List<DepartmentMessage> messages;
    DepartmentMessageViewModel departmentMessageViewModel;
    ImageButton iDepartmentCall;
    String sdepartmentName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("create");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_department, container, false);
        departmentName = view.findViewById(R.id.department_window_department_name);
        messageBox = view.findViewById(R.id.department_chat_window_message_box);
        sendButton = view.findViewById(R.id.department_chat_window_send_button);
        recyclerView = view.findViewById(R.id.department_chat_window_recycler_view);
        iDepartmentCall = view.findViewById(R.id.department_window_calling);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        messages = new ArrayList<>();
        adapter = new DepartmentMessageAdapter(messages,"");
        recyclerView.setAdapter(adapter);
        departmentRepository = new DepartmentRepository(getActivity().getApplication());
        loggedUserRepository = new LoggedUserRepository(getActivity().getApplication());
        departmentMessageViewModel = ViewModelProviders.of(this).get(DepartmentMessageViewModel.class);
        departmentMessageViewModel.getMessages().observe(this, new Observer<List<DepartmentMessage>>() {
            @Override
            public void onChanged(List<DepartmentMessage> departmentMessages) {
                adapter.setMessageList(departmentMessages);
                recyclerView.scrollToPosition(adapter.getItemCount()-1);
            }
        });
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = messageBox.getText().toString();
                if(msg!=""){
                    messageBox.setText("");
                    sendMessage(msg);
                }
            }
        });

        iDepartmentCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) getActivity()).navigateTo(new Calling(loggedUser.getUsername(),sdepartmentName,"group"), true);
            }
        });

        new GtUsers().execute();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void sendMessage(String msg) {
        Date currentDate = new Date();
        SimpleDateFormat date = new SimpleDateFormat("yyyy.MM.dd");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat datetosend = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        String sDate = date.format(currentDate);
        String sTime = time.format(currentDate);
        String sentDate = datetosend.format(currentDate);
        HashMap<String, String> data = new HashMap<>();
        data.put("Message", msg);
        data.put("Sender", loggedUser.getUsername());
        data.put("Department", department.getDepartmentName());
        data.put("Date", sentDate);
        Socket socket=WebSocketService.getSocket();
        if(socket!=null) {

            DepartmentMessage message = new DepartmentMessage();
            message.setMessage(msg);
            message.setTime(sTime);
            message.setDate(sDate);
            message.setSender(loggedUser.getUsername());
            socket.emit("message",new JSONObject(data));
            departmentMessageViewModel.insert(message);
        }
    }
    public void setAdapter(){
        DepartmentMessage departmentMessage = new DepartmentMessage();
        departmentMessage.setMessage("hi");
        departmentMessage.setSender("Image");
        departmentMessage.setDate("2020");
        departmentMessage.setTime("10:00");
        messages.add(departmentMessage);
        messages.add(departmentMessage);
        DepartmentMessage departmentMessages = new DepartmentMessage();
        departmentMessages.setMessage("hi");
        departmentMessages.setSender(loggedUser.getUsername());
      //  System.out.println(loggedUser.getUsername());
        departmentMessages.setDate("2020");
        departmentMessages.setTime("10:00");
        messages.add(departmentMessages);
        adapter.setMessageList(messages);
    }

    private class GtUsers extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... voids) {
            loggedUser = loggedUserRepository.getUser();
            department = departmentRepository.getDepartmentByID(loggedUser.getDepartmentId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            departmentName.setText(department.getDepartmentName());
            sdepartmentName = department.getDepartmentName();
            adapter.setUsername(loggedUser.getUsername());
           // setAdapter();
        }
    }
}
