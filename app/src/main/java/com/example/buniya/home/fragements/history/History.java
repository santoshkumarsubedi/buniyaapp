package com.example.buniya.home.fragements.history;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.buniya.R;
import com.example.buniya.database.entity.HistoryEntity;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.database.viewmodel.HistoryViewModel;
import com.example.buniya.global.GetUpdate;
import com.example.buniya.home.fragements.history.adapter.HistoryAdapter;

import java.util.List;

public class History extends Fragment {
    GetUpdate getUpdate;
    RecyclerView recyclerView;
    HistoryAdapter historyAdapter;
    List<HistoryEntity> histories;
    Activity activity;
    LoggedUserRepository loggedUserRepository;
    LoggedUser loggedUser;
    HistoryViewModel historyViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        recyclerView = view.findViewById(R.id.history_recycler_view);
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);
        getUpdate = new GetUpdate(activity.getApplication());
        historyAdapter = new HistoryAdapter(histories,"",getActivity().getApplicationContext());
        recyclerView.setAdapter(historyAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity.getApplication());
        recyclerView.setLayoutManager(linearLayoutManager);
        GetUpdate getUpdate = new GetUpdate(activity.getApplication());
        getUpdate.UpdateHistory();
        new GetHistory().execute();
        new GetUpdate(getActivity().getApplication()).UpdateHistory();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
    }

    private class GetHistory extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            loggedUserRepository = new LoggedUserRepository(activity.getApplication());
            loggedUser = loggedUserRepository.getUser();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            historyAdapter.setUsername(loggedUser.getUsername());
            historyViewModel.getHistoryData().observe((LifecycleOwner)activity, new Observer<List<HistoryEntity>>() {
                @Override
                public void onChanged(List<HistoryEntity> historyEntities) {
                    historyAdapter.setHistories(historyEntities);
                }
            });
        }
    }
}
