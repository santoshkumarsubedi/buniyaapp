package com.example.buniya.home.fragements.Friends;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.entity.Message;
import com.example.buniya.database.entity.Users;
import com.example.buniya.database.repository.DepartmentRepository;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.database.viewmodel.MessageViewModel;
import com.example.buniya.home.Home;
import com.example.buniya.home.fragements.Friends.adapter.ChatWindowAdapter;
import com.example.buniya.home.fragements.call.Calling;
import com.example.buniya.home.fragements.fileSharing.FileSharing;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.R;
import com.example.buniya.services.WebSocketService;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ChatWindow extends Fragment {

    ImageView backtoHome;
    TextView usernameTextView;
    int user_id;
    RecyclerView recyclerView;
    MessageViewModel messageViewModel;
    DepartmentRepository departmentRepository;
    LoggedUserRepository loggedUserRepository;
    LoggedUser loggedUser;
    Users users;
    ChatWindowAdapter chatWindowAdapter;
    List<Message> messages;
    EditText messageBox;
    ImageView iSendButton,audioCall,fileSharingButton;
    Activity activity;
    public ChatWindow(int user_id){
        this.user_id=user_id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_chat_window, container, false);
        backtoHome = view.findViewById(R.id.chat_window_back_button);
        usernameTextView = view.findViewById(R.id.chat_window_user_name);
        recyclerView = view.findViewById(R.id.chat_window_recycler_view);
        activity = getActivity();
        departmentRepository = new DepartmentRepository(activity.getApplication());
        loggedUserRepository = new LoggedUserRepository(activity.getApplication());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity.getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        chatWindowAdapter = new ChatWindowAdapter("",messages,getActivity().getApplicationContext());
        recyclerView.setAdapter(chatWindowAdapter);
        messageViewModel = ViewModelProviders.of(this).get(MessageViewModel.class);
        messageBox = view.findViewById(R.id.chat_window_message_box);
        iSendButton = view.findViewById(R.id.chat_window_send_button);
        audioCall = view.findViewById(R.id.chat_window_audio_call);
        fileSharingButton = view.findViewById(R.id.chat_window_file_sharing);

        fileSharingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) activity).navigateTo(new FileSharing(loggedUser.getUsername(),users.getUserName()), true);
            }
        });

        backtoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((NavigationHost) getActivity()).navigateTo(new FriendList(), true);
                Intent intent = new Intent(getActivity(), Home.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        new GetUser().execute();

        iSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = messageBox.getText().toString();
                msg=msg.trim();
                if(msg.length()>0) {
                    messageBox.setText("");
                    sendMessage(msg);
                }
            }
        });

        audioCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) activity).navigateTo(new Calling(loggedUser.getUsername(),users.getUserName(),"personal"), true);
            }
        });
        return view;
    }

    public void sendMessage(String msg){
        Date current = new Date();
        SimpleDateFormat datetosend = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        Message message = new Message();
        message.setMessage(msg);
        message.setMessageFrom(loggedUser.getUsername());
        message.setDate_time(current);
        message.setMessageTo(users.getUserName());
        HashMap<String,String> data = new HashMap<>();
        data.put("Message",message.getMessage());
        data.put("Sender",loggedUser.getUsername());
        data.put("SenderId",String.valueOf(loggedUser.getUserId()));
        data.put("Receiver",users.getUserName());
        data.put("ReceiverId",String.valueOf(users.getUserId()));
        data.put("Date",datetosend.format(current));
        Socket socket=WebSocketService.getSocket();
        if(socket!=null) {
            socket.emit("pMessage", new JSONObject(data));
            messageViewModel.Insert(message);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
    }

    private class GetUser extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            loggedUser = loggedUserRepository.getUser();
            users = departmentRepository.getUserById(user_id);
            System.out.println(users.getFullName());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            usernameTextView.setText(users.getFullName());
            chatWindowAdapter.setFriendName(users.getUserName());

            messageViewModel.getListofmessage(users.getUserName()).observe((LifecycleOwner) activity, new Observer<List<Message>>() {
                @Override
                public void onChanged(List<Message> messages) {
                    chatWindowAdapter.setMessages(messages);
                    chatWindowAdapter.getItemViewType(chatWindowAdapter.getItemCount()-1);
                }
            });

        }
    }
}
