package com.example.buniya.home.fragements.call;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.buniya.R;
import com.example.buniya.global.AudioCallFeature;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.services.WebSocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;

public class CallReceiverDialog extends DialogFragment {

    TextView incoming_status;
    public static String TAG="RECEIVINGCALL";
    String port;
    String caller;
    String address;
    String sessionId;
    String type;
    Button bAccept,bReject;
    Socket socket;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if(dialog!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width,height);
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        port = b.getString("PORT");
        caller = b.getString("Caller");
        address = b.getString("IPAddress");
        sessionId = b.getString("SessionId");
        type = b.getString("Type");

        socket = WebSocketService.getSocket();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fullscren_calling_dialog,container,false);
        incoming_status = view.findViewById(R.id.incoming_call_status);
        bAccept = view.findViewById(R.id.incoming_call_accept);
        bReject = view.findViewById(R.id.incoming_call_reject);

        bAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,String> data = new HashMap<>();
                data.put("SessionId",sessionId);
                data.put("Action","accept");
                data.put("Type",type);
                socket.emit("call-action",new JSONObject(data));
                try {
                    AudioCallFeature.address = InetAddress.getByName(address);
                    AudioCallFeature.serverPort = Integer.parseInt(port);
                    AudioCallFeature.startSpeakers();
                    AudioCallFeature.startMic();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }

            }
        });
        socket.on("call-action", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try{
                    System.out.println(args[0]);
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    String action = jsonObject.getString("Action");
                  if(action.equals("reject")){
                        AudioCallFeature.mic = false;
                        AudioCallFeature.speakers = false;
                        Thread.sleep(2000);
                        ((NavigationHost) getActivity()).backTo(false);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        bReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,String> data = new HashMap<>();
                data.put("SessionId",sessionId);
                data.put("Action","reject");
                socket.emit("call-action",new JSONObject(data));
            }
        });
        incoming_status.setText(caller+" is calling you");
        return view;
    }


}
