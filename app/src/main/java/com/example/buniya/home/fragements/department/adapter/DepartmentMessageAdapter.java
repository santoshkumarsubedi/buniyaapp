package com.example.buniya.home.fragements.department.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.buniya.R;
import com.example.buniya.database.entity.DepartmentMessage;

import org.w3c.dom.Text;

import java.util.List;

public class DepartmentMessageAdapter extends RecyclerView.Adapter{
    private List<DepartmentMessage> messages;
    private String username;

    public DepartmentMessageAdapter(List<DepartmentMessage> messages,String username){
        this.messages = messages;
        this.username = username;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType==0){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.incoming_message,parent,false);
            return new IncomingMessageViewHolders(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outgoing_message,parent,false);
            return new OutgoingMessageViewHolders(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(messages!=null){
            DepartmentMessage message = messages.get(position);

            switch (holder.getItemViewType()){
                case 1:
                    ((OutgoingMessageViewHolders) holder).username.setText(message.getMessage());
                    ((OutgoingMessageViewHolders) holder).time.setText(message.getTime());
                    break;
                case 0:
                    ((IncomingMessageViewHolders) holder).username.setText(message.getSender());
                    ((IncomingMessageViewHolders) holder).message.setText(message.getMessage());
                    ((IncomingMessageViewHolders) holder).time.setText(message.getTime());
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        DepartmentMessage message = messages.get(position);
        if(message.getSender().equals(username)){
            return 1;
        }else{
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        if(messages!=null){
            return  messages.size();
        }else{
            return 0;
        }
    }

    public class OutgoingMessageViewHolders extends RecyclerView.ViewHolder{
        private TextView username;
        private TextView time;
        public OutgoingMessageViewHolders(View itemView) {
            super(itemView);
            username=itemView.findViewById(R.id.outgoing_message_username);
            time = itemView.findViewById(R.id.outgoing_message_time);
        }
    }

    public class IncomingMessageViewHolders extends RecyclerView.ViewHolder{
        public TextView username;
        public TextView message;
        public TextView time;
        public IncomingMessageViewHolders(View itemView) {
            super(itemView);
            username=itemView.findViewById(R.id.incoming_message_username);
            message=itemView.findViewById(R.id.incoming_message_message);
            time=itemView.findViewById(R.id.incoming_message_time);
        }
    }

    public void setMessageList(List<DepartmentMessage> messages){
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void setUsername(String username){
        this.username = username;
        notifyDataSetChanged();
    }
}
