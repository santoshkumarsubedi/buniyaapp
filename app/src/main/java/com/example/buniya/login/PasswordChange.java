package com.example.buniya.login;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.buniya.Api.ApiHandler;
import com.example.buniya.Api.RequestPOJO.LoginRequestPOJO;
import com.example.buniya.Api.ResponsePOJO.GeneralResponsePOJO;
import com.example.buniya.R;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.interfaces.ActivityHost;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PasswordChange extends Fragment {

    LoggedUser loggedUser;
    TextInputEditText tIPassword,tIConfirmPassword;
    TextInputLayout tIPasswordLayout,tIConfirmPasswordLayout;
    String sPassword,sConfirmPassword;
    MaterialButton mBNext,mBCancel;
    LoggedUserRepository loggedUserRepository;
    public PasswordChange(LoggedUser loggedUser){
        this.loggedUser = loggedUser;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_password_change, container, false);
       tIPassword = view.findViewById(R.id.change_password_textfield);
       tIConfirmPassword = view.findViewById(R.id.change_re_password_textfield);
       tIPasswordLayout = view.findViewById(R.id.change_password_inputlayout);
       tIConfirmPasswordLayout = view.findViewById(R.id.change_re_password_inputlayout);
       mBNext = view.findViewById(R.id.confirm_password_next_button);
       mBCancel = view.findViewById(R.id.confirm_password_cancel_button);
        loggedUserRepository = new LoggedUserRepository(getActivity().getApplication());

       tIPassword.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {}
           @Override
           public void afterTextChanged(Editable s) {
                sPassword = tIPassword.getText().toString();
                if(sPassword.length()<8){
                    tIPasswordLayout.setError("Password Must be equal or more than 8 Character");
                }else{
                    tIPasswordLayout.setError(null);
                }
                sConfirmPassword = tIConfirmPassword.getText().toString();
           }
       });


       tIConfirmPassword.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {}
           @Override
           public void afterTextChanged(Editable s) {
               sPassword = tIPassword.getText().toString();
               sConfirmPassword = tIConfirmPassword.getText().toString();
               if(!sPassword.equals(sConfirmPassword)){
                   tIConfirmPasswordLayout.setError("Password doesn't match");
               }else{
                   tIConfirmPasswordLayout.setError(null);
               }
           }
       });

       mBNext.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               sConfirmPassword = tIConfirmPassword.getText().toString();
               sPassword = tIPassword.getText().toString();
               if(sPassword.length()<8) {
                   tIPasswordLayout.setError("Password Must be equal or more than 8 Character");
               }else if(!sPassword.equals(sConfirmPassword)){
                   tIConfirmPasswordLayout.setError("Password doesn't match");
               }else{
                   LoginRequestPOJO user = new LoginRequestPOJO();
                   user.setPassword(sPassword);
                   user.setUsername(loggedUser.getUsername());
                   Call<GeneralResponsePOJO> call = ApiHandler.loginInterface.ChangePassword(user,loggedUser.getToken());
                   FragmentActivity activity = getActivity();
                   call.enqueue(new Callback<GeneralResponsePOJO>() {
                       @Override
                       public void onResponse(Call<GeneralResponsePOJO> call, Response<GeneralResponsePOJO> response) {
                           GeneralResponsePOJO responsePOJO = response.body();
                           if(responsePOJO.getStatus().equals("Success")){
                            loggedUserRepository.insert(loggedUser);
                               ((ActivityHost)activity).changeActivity();
                           }
                       }
                       @Override
                       public void onFailure(Call<GeneralResponsePOJO> call, Throwable t) {
                           System.out.println("failed");
                       }
                   });
               }
               }

       });

       return view;
    }

}
