package com.example.buniya.login;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.R;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.util.KeyGenerator;
import com.google.android.material.button.MaterialButton;

import org.json.JSONObject;

import java.util.HashMap;

public class ID_Register extends Fragment {

    KeyGenerator keyGenerator;
    private MaterialButton mByes,mBno;
    String username;

    public ID_Register(String username) {
        this.username = username;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_id__register, container, false);
        mByes = view.findViewById(R.id.verification_yes);
        mBno = view.findViewById(R.id.verification_no);
        keyGenerator = new KeyGenerator();

        mByes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String key = keyGenerator.generateKey();
            }
        });

        mBno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) getActivity()).navigateTo(new Login(), false);
            }
        });

       return view;

    }




}
