package com.example.buniya.login;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.buniya.Api.ApiHandler;
import com.example.buniya.Api.RequestPOJO.LoginRequestPOJO;
import com.example.buniya.Api.ResponsePOJO.LoginResponsePOJO;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.interfaces.ActivityHost;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.R;
import com.example.buniya.global.GlobalValue;
import com.example.buniya.util.KeyGenerator;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login extends Fragment {

    private TextInputLayout lUsernameLayout,lPasswordLayout;
    private TextInputEditText eUsernameEditText,ePasswordEditText;
    private MaterialButton mbLoginNext,mbLoginCancel;
    KeyGenerator keyGenerator;
    String username;
    private LoggedUserRepository loggedUserRepository;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_login, container, false);
        lUsernameLayout = view.findViewById(R.id.login_username_inputlayout);
        lPasswordLayout = view.findViewById(R.id.login_password_inputlayout);
        eUsernameEditText = view.findViewById(R.id.login_username_textfield);
        ePasswordEditText = view.findViewById(R.id.login_password_textfield);
        mbLoginNext = view.findViewById(R.id.login_next_button);
        mbLoginCancel = view.findViewById(R.id.login_cancel_button);
        keyGenerator = new KeyGenerator();
        loggedUserRepository = new LoggedUserRepository(getActivity().getApplication());

        new AutoLoginThread(getActivity()).execute();

        mbLoginNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isPasswordValid(ePasswordEditText.getText())){
                    lPasswordLayout.setError(getString(R.string.invalid_password));
                }else if(!isUsernameValid(eUsernameEditText.getText())){
                    lUsernameLayout.setError("Username must be >5 characters");
                }else{
                    ePasswordEditText.setError(null);
                    lUsernameLayout.setError(null);
                    username = eUsernameEditText.getText().toString();
                    String password = ePasswordEditText.getText().toString();

                    LoginRequestPOJO user = new LoginRequestPOJO();
                    user.setUsername(username);
                    user.setPassword(password);

                    Call<LoginResponsePOJO> call = ApiHandler.loginInterface.Login(user);
                    FragmentActivity hh = getActivity();
                    call.enqueue(new Callback<LoginResponsePOJO>() {
                        @Override
                        public void onResponse(Call<LoginResponsePOJO> call, Response<LoginResponsePOJO> response) {
                            LoginResponsePOJO loginResponse = response.body();
                            if(loginResponse.getStatus().equals("Success")){
                                LoggedUser loggedUser = new LoggedUser();
                                loggedUser.setId(1);
                                loggedUser.setUserId(loginResponse.getUserId());
                                loggedUser.setDepartmentId(loginResponse.getDepartmentId());
                                loggedUser.setFullName(loginResponse.getFullName());
                                loggedUser.setUsername(loginResponse.getUsername());
                                loggedUser.setToken(loginResponse.getToken());
                                GlobalValue.loggedUser=loggedUser;
                                if(loginResponse.getPasswordStatus().equals("YES")){
                                    loggedUserRepository.insert(loggedUser);
                                    ((ActivityHost)hh).changeActivity();
                                }else{
                                    ((NavigationHost) hh).navigateTo(new PasswordChange(loggedUser), false);
                                }

                            }else{
                                Toast.makeText(hh, "Wrong username or Passwordw", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<LoginResponsePOJO> call, Throwable t) {

                        }
                    });
                            //new LoginThread(getActivity(),getContext()).execute(username,ePasswordEditText.getText().toString());

                    ((NavigationHost) getActivity()).navigateTo(new Login(), false);
                }
            }
        });

        mbLoginCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) getActivity()).navigateTo(new Welcome(), false);
            }
        });

        ePasswordEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(isPasswordValid(ePasswordEditText.getText())){
                    lPasswordLayout.setError(null);
                }
                return false;
            }
        });

        return view;
    }

    private boolean isPasswordValid(@Nullable Editable text){
        return text!=null&&text.length()>=5;
    }

    private boolean isUsernameValid(@Nullable Editable text){
        return text!=null&&text.length()>=5;
    }

    class AutoLoginThread extends AsyncTask<String,String,String>{

        FragmentActivity fragmentActivity;
        LoggedUser user;
        public AutoLoginThread(FragmentActivity activity){
            this.fragmentActivity = activity;
        }
        @Override
        protected String doInBackground(String... strings) {
             user = loggedUserRepository.getUser();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(user!=null){
                GlobalValue.loggedUser = user;
                ((ActivityHost)getActivity()).changeActivity();
            }

        }
    }
}
