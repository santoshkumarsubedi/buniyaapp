package com.example.buniya.services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.buniya.R;
import com.example.buniya.database.entity.DepartmentEntity;
import com.example.buniya.database.entity.DepartmentMessage;
import com.example.buniya.database.entity.LoggedUser;
import com.example.buniya.database.entity.Message;
import com.example.buniya.database.repository.DepartmentMessageRepository;
import com.example.buniya.database.repository.DepartmentRepository;
import com.example.buniya.database.repository.LoggedUserRepository;
import com.example.buniya.database.repository.MessageRepository;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.socketio.client.IO;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.security.KeyStore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

public class WebSocketService extends Service{
    private static Socket socket;
    private JSONObject jsonObject;
    private LoggedUserRepository loggedUserRepository;
    private DepartmentRepository departmentRepository;
    private DepartmentMessageRepository departmentMessageRepository;
    private MessageRepository messageRepository;
    DepartmentEntity departmentEntity;
    LoggedUser user;
    String username;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        loggedUserRepository = new LoggedUserRepository(getApplication());
        departmentRepository = new DepartmentRepository(getApplication());
        departmentMessageRepository = new DepartmentMessageRepository(getApplication());
        messageRepository = new MessageRepository(getApplication());
        try {
            SSLContext sslContext = null;
            try {
                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                InputStream inputStream = getApplicationContext().getResources().openRawResource(R.raw.keystore);
                keyStore.load(inputStream,"buniya".toCharArray());

                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory
                                                                                        .getDefaultAlgorithm());
                trustManagerFactory.init(keyStore);
                sslContext = sslContext.getInstance("TLS");
                sslContext.init(null,trustManagerFactory.getTrustManagers(),null);
                HostnameVerifier myHostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };

                IO.Options opts = new IO.Options();
                opts.reconnection = true;
                opts.reconnectionAttempts=100;
                opts.reconnectionDelay=5000;
                //opts.sslContext = sslContext;
                //opts.hostnameVerifier = myHostnameVerifier;
                socket = IO.socket("http://192.168.100.56:3001");
            }catch (Exception e){

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Socket getSocket(){
        if(socket.connected()){
            return socket;
        }else{
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();
        System.out.println("Service is stopped");
        stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new GtUsers().execute();
        return super.onStartCommand(intent, flags, startId);
    }




    public void sendAuthentication(){
        HashMap<String,String> data = new HashMap<>();
        data.put("Username", user.getUsername());
        data.put("UserId",String.valueOf(user.getUserId()));
        data.put("Fullname",user.getFullName());
        jsonObject=new JSONObject(data);
        socket.emit("join_user",jsonObject.toString());
    }

    public void receiveDepartmentMessage(String department){
        socket.on(department, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println(args[0]);
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    DepartmentMessage message = new DepartmentMessage();
                    message.setMessage(jsonObject.get("Message").toString());
                    message.setSender(jsonObject.get("Sender").toString());
                    String date=jsonObject.get("Date").toString();
                    String[] dateandtime=date.split(" ");
                    message.setDate(dateandtime[0]);
                    message.setTime(dateandtime[1]);
                    departmentMessageRepository.insert(message);
                }catch (Exception ex){
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    public void receivePersonalMessage(String username){
        socket.on(username, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                try {
                    System.out.println("message received:"+args[0]);
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    Message message = new Message();
                    message.setMessage(jsonObject.get("Message").toString());
                    message.setDate_time(format.parse(jsonObject.get("Date").toString()));
                    message.setMessageTo(jsonObject.get("Receiver").toString());
                    message.setMessageFrom(jsonObject.get("Sender").toString());
                    messageRepository.insert(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void reciveCall(){
        socket.on("SomeoneCalling", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    String port = jsonObject.getString("PORT");
                    String address = jsonObject.getString("IPAddress");
                    String caller = jsonObject.getString("Caller");
                    String sessionId = jsonObject.getString("SessionId");
                    String type = jsonObject.getString("Type");
                    Intent intent = new Intent();
                    intent.putExtra("PORT",port);
                    intent.putExtra("IPAddress",address);
                    intent.putExtra("Caller",caller);
                    intent.putExtra("SessionId",sessionId);
                    intent.putExtra("Type",type);
                    intent.setAction("Someone.Calling");
                    sendBroadcast(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void receiveDepartmentCall(String department){
        socket.on(department + "-call", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    String port = jsonObject.getString("PORT");
                    String address = jsonObject.getString("IPAddress");
                    String caller = jsonObject.getString("Caller");
                    String sessionId = jsonObject.getString("SessionId");
                    String type = jsonObject.getString("Type");
                    Intent intent = new Intent();
                    intent.putExtra("PORT", port);
                    intent.putExtra("IPAddress", address);
                    intent.putExtra("Caller", caller);
                    System.out.println(caller);
                    intent.putExtra("SessionId", sessionId);
                    intent.putExtra("Type",type);
                    intent.putExtra("Username",username);
                    intent.setAction("Someone.Calling");
                    sendBroadcast(intent);
                }catch(JSONException ex){
                    ex.printStackTrace();
                }
            }
        });
    }


    private class GtUsers extends AsyncTask<LoggedUser,Void,Void> {
        @Override
        protected Void doInBackground(LoggedUser... users) {
            user = loggedUserRepository.getUser();
            departmentEntity = departmentRepository.getDepartmentByID(user.getDepartmentId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!socket.connected()) {
                if (user.getUsername() != null) {
                    socket.connect();
                    socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            sendAuthentication();
                            receiveDepartmentMessage(departmentEntity.getDepartmentName());
                            receivePersonalMessage(user.getUsername());
                            reciveCall();
                            receiveDepartmentCall(departmentEntity.getDepartmentName());
                            username = user.getUsername();
                        }
                    });
                    socket.on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                           // sendAuthentication();
                        }
                    });
                }
            }
        }
    }
}
