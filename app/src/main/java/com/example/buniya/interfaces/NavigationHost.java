package com.example.buniya.interfaces;

import androidx.fragment.app.Fragment;

public interface NavigationHost {
    void navigateTo(Fragment fragment, boolean addToBackstack);
    void backTo(boolean addToBackstack);
}
