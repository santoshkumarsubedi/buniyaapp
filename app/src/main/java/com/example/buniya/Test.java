package com.example.buniya;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;

import com.example.buniya.global.AudioCallFeature;
import com.example.buniya.home.fragements.call.CallReceiverDialog;
import com.example.buniya.interfaces.NavigationHost;
import com.example.buniya.services.WebSocketService;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;

public class Test extends AppCompatActivity {

    TextView incoming_status;
    public static String TAG="RECEIVINGCALL";
    String port;
    String caller;
    String address;
    String sessionId;
    Button bAccept,bReject,bEnd;
    Socket socket;
    String type;
    String username;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscren_calling_dialog);
        Intent intent = getIntent();
        port = intent.getStringExtra("PORT");
        address = intent.getStringExtra("IPAddress");
        sessionId = intent.getStringExtra("SessionId");
        caller = intent.getStringExtra("Caller");
        type = intent.getStringExtra("Type");
        username = intent.getStringExtra("Username");
        socket = WebSocketService.getSocket();

        incoming_status = findViewById(R.id.incoming_call_status);
        bAccept = findViewById(R.id.incoming_call_accept);
        bReject = findViewById(R.id.incoming_call_reject);
        bEnd = findViewById(R.id.incoming_call_end);
        bEnd.setVisibility(View.INVISIBLE);

        bAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,String> data = new HashMap<>();
                data.put("SessionId",sessionId);
                data.put("Action","accept");
                data.put("Type",type);
                if(type.equals("personal")) {
                    socket.emit("call-action", new JSONObject(data));
                }else if(type.equals("group")){
                    data.put("username",username);
                    socket.emit("add-to-department",new JSONObject(data));
                }
                try {
                    AudioCallFeature.address = InetAddress.getByName(address);
                    AudioCallFeature.serverPort = Integer.parseInt(port);
                    AudioCallFeature.startSpeakers();
                    AudioCallFeature.startMic();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }

                bAccept.setVisibility(View.INVISIBLE);
                bReject.setVisibility(View.INVISIBLE);
                bEnd.setVisibility(View.VISIBLE);

            }
        });


        socket.on("call-action", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try{
                    JSONObject jsonObject = new JSONObject(args[0].toString());
                    String action = jsonObject.getString("Action");
                    if(action.equals("reject")){
                        AudioCallFeature.mic = false;
                        AudioCallFeature.speakers = false;
                        finish();
                    }else{
                        System.out.println("something wrong");
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        bReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("personal")) {
                    HashMap<String, String> data = new HashMap<>();
                    data.put("SessionId", sessionId);
                    data.put("Action", "reject");
                    socket.emit("call-action", new JSONObject(data));
                    AudioCallFeature.mic = false;
                    AudioCallFeature.speakers = false;
                }
                finish();
            }
        });

        bEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("group")) {
                    HashMap<String, String> data = new HashMap<>();
                    data.put("SessionId", sessionId);
                    data.put("username", username);
                    socket.emit("remove-from-department", new JSONObject(data));

                }else if(type.equals("personal")){
                    HashMap<String, String> data = new HashMap<>();
                    data.put("SessionId", sessionId);
                    data.put("Action", "reject");
                    socket.emit("call-action", new JSONObject(data));
                }
                AudioCallFeature.mic = false;
                AudioCallFeature.speakers = false;
                finish();
            }
        });


        incoming_status.setText(caller+" is calling you");

       /* System.out.println(intent);
        Bundle b = new Bundle();
        b.putString("PORT",port);
        b.putString("IPAddress",address);
        b.putString("SessionId",sessionid);
        b.putString("Caller",caller);
        System.out.println(caller);
        CallReceiverDialog dialog = new CallReceiverDialog();
        dialog.setArguments(b);
        FragmentTransaction fragmentTransaction =getSupportFragmentManager().beginTransaction();
        dialog.show(fragmentTransaction,CallReceiverDialog.TAG);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("Destroy");
    }
}
